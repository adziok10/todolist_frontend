const changeState = new CustomEvent('changeState', { a: 'xd' });


export class LocalStore {

    constructor() {
        const delegate = document.createDocumentFragment();
        [
            'addEventListener',
            'dispatchEvent',
            'removeEventListener'
        ].forEach(f =>
            this[f] = (...xs) => delegate[f](...xs)
        )
    }

    get(key) {
        return key && localStorage.getItem(key) || localStorage
    }

    set(key, val) {
        localStorage.setItem(key, val)
    }
    

    setToken(access_token, refresh_token) {
        this.set('access_token', access_token);
        this.set('refresh_token', refresh_token);

        this.dispatchEvent(changeState, this);
    }

}