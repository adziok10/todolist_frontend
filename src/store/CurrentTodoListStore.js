import axios from 'axios';

import { Effect, createAction, createSubStore } from './GlobalStore';
import { getValOrDefault } from '../utils'

// export const GetAllTodoListsAction = createAction('GET_ALL_TODOLISTS');
export const AddTodoAction = createAction('ADD_TODO');

// const getAllTodoEffect = new Effect(GetAllTodoListsAction, (s, f, { accessToken }) => {
//     axios.get('http://localhost:4000/' + 'api/todolist', {
//         headers: {
//             Authorization: `Bearer ${accessToken}`
//         }
//     })
//         .then(({ data }) => s(data))
//         .catch(err => f(err));
// })
// .onSuccess(({ data, updateStore }) => {
//     updateStore({ todoLists: data, loading: false });
// })
// .onFailure(({ updateStore }) => updateStore({ todoLists: null, loading: false }))

const addTodoEffect = new Effect(AddTodoListAction, (s, f, { accessToken, title, todoList }) => {
        axios.post('http://localhost:4000/' + 'api/todo', {
            title,
            todoList,
        }, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        })
            .then(({ data }) => s(data))
            .catch(err => f(err));
})
.onSuccess(({ data, updateStore }) => {
    updateStore(data);
})
.onFailure(err => console.log(err))
.applyReducer((state, newState) => ({
    ...state,
    todoList: {
        ...state.todoList || {},
        todos: [...getValOrDefault(state, 'todoList.todos', []), newState]
    }
}))

export const TodoStore = createSubStore([AddTodoAction], [addTodoEffect]);