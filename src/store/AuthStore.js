import axios from 'axios';

import { Effect, createAction, createSubStore } from './GlobalStore';

export const LoginAction = createAction('LOGIN');
export const LoginCheckAction = createAction('LOGIN_CHECKER');

const loginEffect = new Effect(LoginAction, (s, f, { email, password }) => {
    axios.post('http://localhost:4000/' + 'api/auth/login', {
        email,
        password
    })
        .then(({ data }) => s(data))
        .catch(err => f(err));
})
    .onSuccess(({ data, updateStore }) => {
        localStorage.setItem('access_token', data.access_token);
        updateStore({ access_token: data.access_token });
    })
    .onFailure(err => console.log(err))

const loginCheckEffect = new Effect(LoginCheckAction, (s, f) => {
    const at = localStorage.getItem('access_token');

    at && s(at) || f();
})
    .onSuccess(({ data, updateStore }) => {
        updateStore({ access_token: data });
    })
    .onFailure(err => {})

export const AuthStore = createSubStore([LoginAction, LoginCheckAction], [loginEffect, loginCheckEffect])