import { GlobalStore } from './GlobalStore';
import { AuthStore } from './AuthStore'
import { TodoListStore } from './TodoListStore'
// import { CurrentTodoListStore } from './CurrentTodoListStore'

export const initStore = () => {
    const store = new GlobalStore();
    store.add('auth', AuthStore);
    store.add('todoList', TodoListStore);
    // store.add('currentTodoList', CurrentTodoListStore);

    return store;
}
