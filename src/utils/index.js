
export const getValOrDefault = (obj = {}, path = [], def = undefined) => {
    const keys = path.split('.');

    const goDeeper = (o) => {
        let key = keys.shift()

        return o[key] && keys.length > 0 && goDeeper(o[key]) 
            || o[key] && keys.length === 0 && o[key] 
            || def
    };
    return goDeeper(obj)
};